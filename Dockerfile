FROM openjdk:11-jdk
ADD target/user-1.0.jar user.jar
ENV TZ=Europe/Moscow
ENTRYPOINT exec java $JAVA_OPTS -jar /user.jar