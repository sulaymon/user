package com.dev.tenet.hackaton;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping("/user/checkTransferLimit/{userId}")
    public boolean checkIfTransferLimitIsNotExceeded(@PathVariable("userId") Integer userId) {
        return userService.checkIfTransferLimitIsNotExceeded(userId);
    }

}
